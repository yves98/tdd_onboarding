class Bigbang:
    def process(self, n):
        if (n > 0) and (n % 5) == 0:
            return "big" if n%2 != 0 else "gib"
        if (n > 0) and (n % 7) == 0:
            return "bang" if n%2 != 0 else "gnab"
        if (n > 0) and (n % 11) == 0:
            return "boom" if n%2 != 0 else "moob"
        return n

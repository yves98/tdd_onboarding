from bigbang import Bigbang
import math

def test_process_default():
    for i in range(5):
        assert Bigbang().process(i) == i

def test_process_five_multiples():
    assert Bigbang().process(5) == "big"

def test_process_five_multiples():
    assert Bigbang().process(7) == "bang"
    
def test_process_eleven_multiples():
    assert Bigbang().process(11) == "boom"
    
def test_process_five_even_multiples():
    assert Bigbang().process(10) == "gib"
    
def test_process_seven_even_multiples():
    assert Bigbang().process(14) == "gnab"
    
def test_process_eleven_multiples():
    assert Bigbang().process(44) == "moob"

# bonus
def test_process_combined_five_seven_multiples():
    assert Bigbang().process(35) == "big"
    
def test_process_combined_five_seven_even_multiples():
    assert Bigbang().process(70) == "gib"

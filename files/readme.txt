C’est tout simple, il faut créer une fonction bigBang qui accepte un entier en paramètre et qui renvoie cet entier.

Mais si on reçoit un nombre multiple de 5, on renvoie big. 
Si c’est un multiple de 7, on renvoie bang. 
Et si c’est un multiple de 11, on renvoie boom
Et si c’est pair, on inverse les mots.
